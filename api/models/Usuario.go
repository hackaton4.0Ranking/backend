package models

import (
	"errors"
	"time"

	"customware.com.ar/hacktonranking/api/utils"
	"gorm.io/gorm"
)


type Usuario struct {
	ID         int64  `gorm:"primaryKey;autoIncrement" json:"id"`
	Email      string `gorm:"column:email;type:varchar(300)" json:"email"`
	Pass       string `gorm:"column:pass" json:"pass"`
	Sesion      string `gorm:"column:token"`
	Token      string `gorm:"column:token"`
	Datos      JSON   `gorm:"column:datos" json:"datos"`
	Creado     string `gorm:"column:creado;type:varchar(30)" json:"creado" `
	Modificado string `gorm:"column:modificado;type:varchar(30)" json:"modificado"`
	Tipo       string `gorm:"column:tipo;default:P;type:varchar(1)" json:"tipo"`
	Status     string `gorm:"column:status;default:A;type:varchar(1)" json:"status"`
}

// usado por gorm
func (u *Usuario) TableName() string {
	return "usuarios"
}

// valores automáticos y por defecto
func (u *Usuario) Preparar() {

	if u.Creado == "" {
		u.Creado = string(time.Now().UTC().Format(utils.FORMATO_TIMESTAMP))
	}
	u.Modificado = string(time.Now().UTC().Format(utils.FORMATO_TIMESTAMP))
	
	if u.Tipo == "" || (u.Tipo != "A" && u.Tipo != "P") {
		u.Tipo = "P"
	}
	
	if u.Status == "" || (u.Status != "I" && u.Status != "A") {
		u.Status = "A"
	}
}

func (u *Usuario) Validar(escenario string, db *gorm.DB) (error) {
	if escenario == utils.ESCENARIO_CREA || escenario == utils.ESCENARIO_MODIFICA{
		if u.Email == "" {
			return errors.New("email requerido")
		}
	}
	return nil
}

// crear un usuario
func (u *Usuario) Crear(db *gorm.DB) (error) {
	var err error
	u.Preparar()
	err = db.Debug().Create(&u).Error
	if err != nil {
		return err
	}
	return nil
}

// buscar un usuario por sesion
func (s *Usuario) ObtenerPorSesion(db *gorm.DB) (*Usuario, error) {
	err := db.Debug().Model(Usuario{}).
		Where("sesion = ?", s.Sesion).Take(&s).Error
	return s, err
}