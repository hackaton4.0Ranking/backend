package utils

import (
	"errors"
	"strings"
)

// dar formato a mensajes de error
func FormatearError(err string) error {

	if strings.Contains(err, "nombre_repetido") {
		return errors.New("el nombre ya existe")
	}

	return errors.New("error desconocido")
}
