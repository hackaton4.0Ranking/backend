package utils

import (
	"net/http"
)

func SetMiddlewares(next http.HandlerFunc) http.HandlerFunc {
	return SetMiddlewareCORS(SetMiddlewareJSON(next))
}

// cambia el tipo de retorno a json
func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	}
}

// setea lo necesario para cors
func SetMiddlewareCORS(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == http.MethodOptions {
			return
		}
		next(w, r)
	}
}
