package api

import (
	"log"

	"customware.com.ar/hacktonranking/api/controllers"
	"customware.com.ar/hacktonranking/api/models"
	"gorm.io/gorm"
)

var server = controllers.Server{}

// inicia la API
func Run() {

	server.Initialize("sqlite", "", "", "", "", "db/base.db")

	Load(server.DB)

	server.Run(":8080")

}

// migración de base de datos
func Load(db *gorm.DB) {
	err := db.Debug().AutoMigrate(
		&models.Usuario{},
	)
	if err != nil {
		log.Fatalf("no se pudo migrar tabla: %v", err)
	}
}