package controllers

import (
	"net/http"

	"customware.com.ar/hacktonranking/api/utils"
	"github.com/gorilla/mux"
)

// generar la rutas de la API
func (s *Server) initializeRoutes() {
	// estado y versión del servicio
	s.Router.HandleFunc("/", utils.SetMiddlewares(s.Home)).Methods(http.MethodGet, http.MethodOptions)

	// gestión de usuarios
	s.Router.HandleFunc("/usuarios", utils.SetMiddlewares(s.NuevoUsuario)).Methods(http.MethodPost, http.MethodOptions)

	s.Router.Use(mux.CORSMethodMiddleware(s.Router))
}
