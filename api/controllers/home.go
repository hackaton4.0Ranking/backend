package controllers

import (
	"net/http"

	"customware.com.ar/hacktonranking/api/utils"
)

// estado y versión del servicio
func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	utils.JSON(w, http.StatusOK, "Hackaton 4.0 Ranking API 1.0.0")
}
