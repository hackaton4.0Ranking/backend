package controllers

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"customware.com.ar/hacktonranking/api/models"
	"customware.com.ar/hacktonranking/api/utils"
)

func (server *Server) NuevoUsuario(w http.ResponseWriter, r *http.Request) {
	if len(r.Header["Authorization"]) > 0 {
		sesion := models.Usuario{}
		sesion.Sesion = strings.Replace(r.Header["Authorization"][0], "Bearer ", "", -1)
		_, err := sesion.ObtenerPorSesion(server.DB)
		if err != nil {
			utils.ERROR(w, http.StatusUnauthorized, err)
			return
		}
		if sesion.ID == 0 || sesion.Status != "A" || sesion.Tipo != "A" {
			utils.ERROR(w, http.StatusUnauthorized, errors.New("sesion_invalida"))
			return
		}
	} else {
		utils.ERROR(w, http.StatusUnauthorized, errors.New("sesion_invalida"))
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	usuario := models.Usuario{}

	err = json.Unmarshal(body, &usuario)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	err = usuario.Validar(utils.ESCENARIO_CREA, server.DB)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	err = usuario.Crear(server.DB)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	utils.JSON(w, http.StatusOK, map[string]interface{}{
		"resultado": "OK",
	})
}